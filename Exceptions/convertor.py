import csv
import pandas as pd
import sys
import os

TEST_PATH_CSV = r'/Users/nico/PycharmProjects/ITC_Python/Exceptions/test.csv'
TEST_PATH_TXT = r'/Users/nico/PycharmProjects/ITC_Python/Exceptions/test.txt'
TEST_PATH_XLS = r'/Users/nico/PycharmProjects/ITC_Python/Exceptions/test.xlsx'
TEST_EMPTY_CSV = r'/Users/nico/PycharmProjects/ITC_Python/Exceptions/test_empty.csv'


def line_to_value(csv_line: list):
    """
    Breaks the csv_line into list of values
    :param csv_line: list of one string where values are separated by ;
    :return: a list of values
    """
    columns = csv_line[0].strip(';').split(';')
    if len(columns) > 1:
        raise ValueError('The file contains more than one column')
    value = columns[0]
    return value


def xls_to_csv(path_to_file: str):
    """
    Function that converts the xls/ xlsx file into a csv file
    :param path_to_file: absolute path to the excel file
    :return: path to the newly created csv file
    """
    xls_file = pd.read_excel(path_to_file, index_col=None)
    xls_file.to_csv(''.join(path_to_file.split('.')[:-1])+'.csv', encoding='utf-8', index=False)
    return ''.join(path_to_file.split('.')[:-1]) + '.csv'


def file_suppression(path_to_file):
    os.remove(path_to_file)
    print('File deleted!')


def convertor(path_to_file: str):
    """
    Function that opens the csv provided and convert the meters data into feet data
    :param path_to_file: path to the csv file
    :return: Nothing: create a another file with the suffix "_new" with the conversion and exit
    """
    was_excel = False

    if path_to_file.split('.')[-1] in ['xls', 'xlsx']:
        path_to_file = xls_to_csv(path_to_file)
        was_excel = True

    elif path_to_file.split('.')[-1] != 'csv':
        raise ValueError('The file provided is not in the correct format, please provide a .csv/.xlsx/.xls file')

    try:
        file = open(path_to_file, 'r')

        # do not touch the title of the file
        file.seek(0)
        csv_reader = csv.reader(file)

        for i, line in enumerate(csv_reader):
            # skip the title
            if i > 0:
                # if the line is only ';'
                if len(line[0]) == 1:
                    raise ValueError('The file contains empty lines')

                value = line_to_value(line)

                if not isinstance(value, int) or not isinstance(value, float):
                    if value.isdigit():
                        value = float(value)
                    else:
                        raise ValueError('The file has non numerical entries')

                if value < 0:
                    raise ValueError('The file contains incorrect negative entries')

        file.seek(0)
        csv_reader = csv.reader(file)

        new_file = open(''.join(path_to_file.split('.')[:-1]) + '_new.' + path_to_file.split('.')[-1], 'w')
        csv_writer = csv.writer(new_file, delimiter=';', lineterminator='\n')

        for i, csv_line in enumerate(csv_reader):
            # to skip the title
            if i > 0:
                csv_writer.writerow(csv_line + [str(float(csv_line[0]) * 3.2808399)])

        file.close()
        new_file.close()

        # If the file was an excel file, we convert the file back from csv to excel
        if was_excel:

            # Create a excel file from the csv (ok I only output xlsx file...)
            tmp_df = pd.read_csv(path_to_file)
            excel_writer = pd.ExcelWriter( ''.join(path_to_file.split('.')[:-1])+'_new.xlsx', engine='xlsxwriter')
            tmp_df.to_excel(excel_writer, sheet_name='Sheet1')
            excel_writer.save()

            # delete the temporary csv created
            file_suppression(path_to_file)

            # delete the new csv we created (Ok, it could have been more opti...)
            file_suppression(''.join(path_to_file.split('.')[:-1]) + '_new.csv')

            print('Finally...')

        print('Job done!')

    except FileNotFoundError:
        print('The file you are trying to open does not exist, check the path again')

    except ValueError as error:
        print(error.args[0])



def main():
    if True:
        path_to_file = r'/Users/nico/PycharmProjects/ITC_Python/Exceptions/test.xlsx'
    else:
        path_to_file = sys.argv[1:]
        if len(path_to_file) != 1:
            path_to_file = str(input('Please provide another path'))
    convertor(path_to_file)

if __name__ == "__main__":
    main()
