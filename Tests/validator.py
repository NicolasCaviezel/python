def safe_is_digit(input_digit):
    """
    Safe extended version of character built-in "isdigit()"
    :param input_digit: any type of input
    :return: True if is it s a character cast able in digit False otherwise
    """
    input_digit = str(input_digit)
    try:
        return input_digit.isdigit()
    except AttributeError:
        return False


def numbers_multiplier(numbers: str):
    """
    This function returns the multiplication of input numbers if it exists
    :param numbers: a string of spaced separated numbers
    :return: the multiplication of the input numbers or an error message
    """
    result = 1
    for number in numbers.split():
        if number[0] == '-':
            result = -result
            number = number[1:]
        result *= int(number)
    return result


def check_input(input_numbers):
    """
    Check if input_numbers is of correct form to perform the multiplication
    :param input_numbers:
    :return: True if the input is of correct form, False otherwise
    """
    input_numbers = input_numbers.split()
    if len(input_numbers) == 0:
        return False
    for number in input_numbers:
        if number[0] == '-':
            number = number[1:]
        if not safe_is_digit(number):
            return False
    return True


def main():
    """
    Ask the user to enter a string spaced numbers to multiply
    :return: print the result of the multiplication and exit
    """
    numbers_to_multiply = str(input("Your string-->"))
    is_valid = check_input(numbers_to_multiply)
    while not is_valid:
        numbers_to_multiply = str(input("Please provide a valid input "))
        is_valid = check_input(numbers_to_multiply)
    print(numbers_multiplier(numbers_to_multiply))


if __name__ == '__main__':
    assert numbers_multiplier('6') == 6
    assert numbers_multiplier('1 2 3 4 5') == 120
    assert numbers_multiplier('-1 -2 -3') == -6
    assert check_input(' 1 2 3 0')
    assert not check_input('[1] 2')
    assert not check_input('a b 1 2')
    assert not check_input(' ')
    main()
