from string import digits


DELIMITER = " "
CRYPTED_TEXT = "YXu0hYq1xQ4,hQXUCw8Cs7nIu8BAxIUADYf\n\nLRYUYw5LI0RYUUYs6hDXU3UIoIBbcm\n.CBSA5LI7RYUUYs2h" \
               "DXU8YBbnIj\n.UIoIBbnI2LI8RYUUYs7hDXU2cYBbnQZ\n.cYBbnQo1LI9RYUUYs7hDXU4UDBk\n.JYUDoIBbnQ" \
               "o4LI3RYUUYs8hDXU6YLRDbj\n.JYULYh8LI3RYUUYs8hDXU1CUIBIsDJDYH\n.YLhYJ7BDIoYbj\n.LUhAQo0LY" \
               "LDo3U'hYRD2BDIoYbL2XSAQhY0QU0MDYRs0YXU4XSAQXUBW\n.LYBAR8CUIBDoIUoDRb1LUDYs2LRQRRm\n.CUIR" \
               "Ab3JBAQXL6RYlYh9LLDb0LLYBha\n.CBUhYBIL0CBUIoIBbcY9hg\n.JYohYBIL1YXU5YoDx7xQ6,CUIASIsnD8Y" \
               "LAxYR0YXU9hQIUDUbnYU4QU3YRYXu\n.LLYAS1JBAQXL7Ys3--YhQ2JhD2CBsDRYxYRb2CBhQ9YhQ6LAQIlsQ--9" \
               "CDO7QU1QJ0XSAQXUBW\n.UI4UDXU5CDO0CDn2UQh7Ys2LAQIlsQ0UD2ULRIx8LLYBhA2YR'AQC1OQe\n.XoUAv7" \
               "LI3RYUUYs0hDXU8XSAQXUBW\n.RYlYh0RYlYh5LI9hYUxQ0RYUUYs2hDXU8*UXSIR*9xg\n.OQh4YXU3hQIUDUhYn" \
               "YBbnI9LI1JRDX0QU6,hIDBbcY5L'UI3D7JDs6xg\n.DYJI4YXU6hQIUDUhYnYBbnI9LI9CLDY4QU3,hIDBbcY2UI5" \
               "CDn4Ys8D8JQQS1LYoDbLYnDe\n.DYJI0YRD9YhQ6ShIMhQX2UDYRS4DYJI3--9L'UYB5QJ8YRQn9xQ0!YLQXU"


def make_uncryption_dictionary(dictionary: dict):
    """

    :param dictionary: cryption dictionary we want to revert
    :return: revert dictionary
    """
    return {value:key for (key, value) in dictionary.items()}


def decrypt_letters(crypted_text: str, uncryption_dictionary: dict):
    """

    :param crypted_text:
    :param uncryption_dictionary:
    :return: a letters uncyption version of the text of type str
    """
    return "".join([uncryption_dictionary[crypted_letter] if crypted_letter in uncryption_dictionary else crypted_letter
                    for crypted_letter in crypted_text])

def is_digit(character):
    """

    :param character: the query character we test if it is a digit
    :return: True: the character is a digit / False: the character is not
    """
    if character in ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]:
        return True
    return False


def replace_digit_by_space(crypted_space_text: str):
    """

    :param crypted_space_text: text of type str we want to replace digits by DELIMITER
    :return: a string where
    """
    return "".join([DELIMITER if letter in digits else letter for letter in crypted_space_text])


def reverse_word(word: str):
    """

    :param word: string we want to revert
    :return: reverted string
    """
    reverted_word = ""
    for i in range(len(word)):
        reverted_word += word[len(word) - 1 - i]
    return reverted_word


def reverse_text(unreversed_text: str):
    """

    :param unreversed_text: string we want to revert
    :return: the reverted string
    """
    uncrypted_text = ""

    # Split the sentence word by word
    list_of_words = unreversed_text.split(" ")

    # Reverse all words
    for i in range(len(list_of_words)):
        list_of_words[i] = reverse_word(list_of_words[i])

    # Recreate the text
    for i in range(len(list_of_words)):
        uncrypted_text += list_of_words[i]
        if i < len(list_of_words) - 1:
            uncrypted_text += " "

    return uncrypted_text


def main():

    cryption_dictionary = {'s': 'L', 'b': 's', 'w': 'O', 'z': 'G', 'c': 'o', 'J': 'y', 'V': 't', 'P': 'w', 'B': 'f',
                           'Z': 'q', 'F': 'k', 'O': 'N', 'u': 'A', 'W': 'r', 'K': 'K', 'a': 'D', 'v': 'l', 'g': 'S',
                           'f': 'x', 'x': 'c', 'N': 'e', 'p': 'b', 'U': 'a', 'j': 'P', 'o': 'Q', 'i': 'I', 'M': 'd',
                           't': 'U', 'H': 'V', 'X': 'i', 'Y': 'T', 'R': 'H', 'h': 'X', 'L': 'z', 'G': 'F', 'A': 'W',
                           'm': 'n', 'T': 'u', 'l': 'B', 'C': 'Z', 'q': 'p', 'D': 'v', 'I': 'g', 'n': 'h', 'y': 'C',
                           'S': 'j', 'k': 'M', 'd': 'J', 'Q': 'E', 'e': 'Y', 'r': 'R', 'E': 'm'}

    uncryption_dictionary = make_uncryption_dictionary(cryption_dictionary)

    uncrypted_letters_text = decrypt_letters(crypted_text, uncryption_dictionary)

    digit_uncrypted_text = replace_digit_by_space(uncrypted_letters_text)

    uncrypted_text = reverse_text(digit_uncrypted_text)

    # Print for debugging
    print(uncrypted_text)


if __name__ == "__main__":
    main()
