from functools import reduce

LYRICS_PATTERN = ["to you\n", "to you\n", "dear", "to you"]
SIMON_BIRTHDAY = "Happy Birthday to you\nHappy Birthday to you\nHappy Birthday dear Simon\nHappy Birthday to you"


def happy_birthday(name: str):
    """
    Function that generates the birthday song of a person
    :param name: person's name of type str
    :return: the birthday song of type string
    """
    full_lyrics = list(map(lambda sentence : 'Happy Birthday ' + sentence, LYRICS_PATTERN))
    return reduce(lambda past_sentence, next_sentence:
                  past_sentence + next_sentence + ' '
                  + name + '\n' if next_sentence.split(' ')[-1] == 'dear'
                  else past_sentence + next_sentence, full_lyrics)


def main():
    assert happy_birthday('Simon') == SIMON_BIRTHDAY
    print(happy_birthday('Simon'))


if __name__ == "__main__":
    main()
