# The solution depends on wether we want UNITED KINGDOM to be United Kingdom or United kingdom

world_map = lambda countries: list(map(lambda country: country.title(), countries))

# or
# world_map = lambda countries: list(map(lambda country: country.capitalize(), countries))
