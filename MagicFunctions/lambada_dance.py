from functools import reduce

div_mod = lambda a, b: (a / b, a % b)
non_negative = lambda a: int(a >= 0) * a
double_digits = lambda a: int(reduce(lambda x, y: x + y, map(lambda c: c*2, str(a))))

#step1 = lambda a: list(str(a))
#step2 = lambda character: character + character
#step3 = lambda a: list(map(step2, step1(a)))
#step4 = lambda a: reduce(lambda x, y: x+y, step3(a))
#step5 = lambda a: int(step4(a))

#double_digits = lambda a: step5(a)
