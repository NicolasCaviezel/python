world_map = lambda countries: list(map(lambda country: country.title(), countries))

filtered_world_map = lambda countries: list(filter(
    lambda country: (country[0] in ['F', 'I']) or (country[-1] in ['d', 'm']),
    world_map(countries)))

assert filtered_world_map(['ISRAEL', 'france', 'engLand', 'Brazil']) == ['Israel', 'France', 'England']
