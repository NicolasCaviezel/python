SOURCE_FILE = "/Users/nico/PycharmProjects/ITC_Python/Dictionaries&Files/source_folder/source_file.txt"
SOURCE_IMAGE = "/Users/nico/PycharmProjects/ITC_Python/Dictionaries&Files/source_folder/source_image.jpeg"
TARGET_FOLDER = "/Users/nico/PycharmProjects/ITC_Python/Dictionaries&Files/target_folder"


def copaste(path1: str, path2: str):
    """
    Copy the content of the text file located in path1 to the path2 folder
    :param path1: an absolute path to a text file
    :param path2: an absolute path to a folder
    :return: None
    """
    try:
        with open(path2 + '/' + path1.split('/')[-1], 'wb') as copied_file:
            with open(path1, 'rb') as file_to_copy:
                copied_file.write(file_to_copy.read())
    except IsADirectoryError:
        print("The path to the file is a path to a directory")
    except FileNotFoundError:
        print("One or both of the path provided don't exist")


def main():
    source_file_path = input("Path to the file to copy")
    target_folder_path = input("Path to the folder where you want to copy the file")
    copaste(source_file_path, target_folder_path)


if __name__ == "__main__":
    copaste(SOURCE_FILE, TARGET_FOLDER)
    with open(TARGET_FOLDER + '/source_file.txt', 'r') as target_file:
        with open(SOURCE_FILE, 'r') as source_file:
            assert target_file.read() == source_file.read()

    copaste(SOURCE_IMAGE, TARGET_FOLDER)
    with open(TARGET_FOLDER + '/source_image.jpeg', 'rb') as target_image:
        with open(SOURCE_IMAGE, 'rb') as source_image:
            assert target_image.read() == source_image.read()

    main()
