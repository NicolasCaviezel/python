#!/usr/bin/python -tt
##   adapted to Python3 for ITC - 17/10/18
##       - also added pep8 and naming convention compliance
##   instructions were changed to deal with proper handling of punctuation   
##
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

"""Wordcount exercise
Google's Python class

The main() below is already defined and complete. It calls print_words()
and print_top() functions which you write.

1. For the --count flag, implement a print_words(filename) function that counts
how often each word appears in the text and prints:
word1 count1
word2 count2
...

Print the above list in alphabetical order. Store all the words as lowercase,
so 'The' and 'the' count as the same word.

2. Remember to deal with punctuation. For example:
Alice     Alice:     "Alice      Alice,    Alice"   are all the same word   >>>> alice

BUT - In words like  Alice's   or   they're, the apostrophe is part of the word
so don't split them into into    Alice + s    or   they  +  re

3. For the --topcount flag, implement a print_top(filename) which is similar
to print_words() but which prints just the top 20 most common words sorted
so the most common word is first, then the next most common, and so on.

Use str.split() (no arguments) to split on all whitespace.

Workflow: don't build the whole program at once. Get it to an intermediate
milestone and print your data structure and sys.exit(0).
When that's working, try for the next milestone.

4. Please use functions to prevent writing duplicate code.

In addition to  print_words(filename) and print_top(filename) functions,
you must write additional functions that read a file, build a word/count dict
and so on.


"""

import sys
import string

# constants used for main()
REQUIRED_NUM_OF_ARGS = 3
ARG_OPTION = 1
ARG_FILE_NAME = 2


# +++your code here+++
def word_reduction(word: str):
    """
    Function that returns the word without [" , . :]
    :param word: of type string
    :return: the lowercase reduced word
    """
    table = str.maketrans({key: None for key in string.punctuation})
    return word.translate(table).lower()


def extract_words(file_name: str):
    """
    Function that reads a file, returns a list of words in it
    :param file_name: absolute path to the file
    :return: list of words
    """
    try:
        with open(file_name, 'r') as f:
            words = f.read().split()
            return words
    except FileNotFoundError:
        print('The file you try to reach does not exist')


def count_words(words: list):
    """
    Function that take count the appearance of each words within a list given as parameter
    :param words: list of words
    :return: dictionary of {word: count}
    """
    word_dictionary = {}
    for word in words:
        # reduce the word to lower case without [, : " .]
        tmp_word = word_reduction(word)
        if tmp_word in word_dictionary:
            word_dictionary[tmp_word] += 1
        else:
            word_dictionary[tmp_word] = 1
    return word_dictionary


def print_words(file_name: str):
    """
    Function that prints the count of each word in the text following the conventions described
    above.
    :param file_name: absolute path to the file, of type string
    :return: the list of counted_words in alphabetical order and the list of word_counts
    """
    # Read the file and load it s content
    words = extract_words(file_name)

    # Count the appearance of each words
    word_dictionary = count_words(words)

    # Make list of counted_words and word_count
    counted_words = [None] * len(word_dictionary)
    word_count = [None] * len(word_dictionary)
    item = list(word_dictionary.items())
    item.sort()
    for i in range(len(item)):
        counted_words[i], word_count[i] = item[i]
        print("%s: %d " % (counted_words[i], word_count[i]))

    return counted_words, word_count


def print_top(file_name: str):
    """
    Function that prints the top 2O words according to their appearance
    :param file_name: absolute path to the file
    :return: the list of the top 20 counted_words and the list of their count
    """
    # Extract words
    words = extract_words(file_name)

    # Count words
    word_dictionary = count_words(words)

    # Print top 20 words according to their appearance
    item = list(word_dictionary.items())
    item.sort(key=lambda x: x[1], reverse=True)
    counted_words = [None] * 20
    word_count = [None] * 20
    for i in range(20):
        counted_words[i], word_count[i] = item[i]
        print("%s: %d " % (counted_words[i], word_count[i]))

    return counted_words, word_count


###

# This basic command line argument parsing code is provided and
# calls the print_words() and print_top() functions which you must define.
def main():
    """
    get user input - file name and which option --cout or --topcount
    read text file, count and sort words
    """

    if len(sys.argv) != REQUIRED_NUM_OF_ARGS:
        print("usage: ./wordcount.py {--count | --topcount} file")
        sys.exit(1)

    option = sys.argv[ARG_OPTION]
    filename = sys.argv[ARG_FILE_NAME]
    if option == "--count":
        # NOTE - change to original Google template on next line
        counted_words, word_count = print_words(filename)
    elif option == "--topcount":
        # NOTE - change to original Google template on next line
        counted_words, word_count = print_top(filename)
    else:
        print("unknown option: " + option)
        sys.exit(1)


if __name__ == "__main__":
    main()
