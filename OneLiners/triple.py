def triple(string: str):
    """
    Function that take a string and returns another string with all words appearing 3 times
    :param string: input_string
    :return: output tripled string
    """
    return ''.join([char * 3 for char in string])


def main():
    string = str(input('Please insert a string'))
    print('The tripled string is : ', triple(string))


if __name__ == '__main__':
    assert not triple('')
    assert triple('abc') == 'aaabbbccc'
    assert triple('a 1') == 'aaa   111'
    main()