def spaced_triple(string: str):
    """
    Function that returns the string where each words of input string appears 3 times
    :param string: input string
    :return: tripled string
    """
    return ' '.join([word*3 for word in string.split()])


def main():
    string = str(input('Please enter your string'))
    print('The spaced triple string is:', spaced_triple(string))


if __name__ == '__main__':
    assert not spaced_triple(' ')
    assert spaced_triple('hello') == 'hellohellohello'
    assert spaced_triple('hello hallo') == 'hellohellohello hallohallohallo'
    assert spaced_triple(' hello ') == 'hellohellohello'
    main()