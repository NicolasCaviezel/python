CHARS_TO_CLEAN = 'cofe'


def cleaning_right(strings: list):
    """
    Function that delete the right part of a string containing a mix of 'c' 'o' 'f' 'e'
    :param strings: input list of string to clean
    :return: cleaned string
    """
    return [string.strip(CHARS_TO_CLEAN) for string in strings]


def main():
    assert cleaning_right(['']) == ['']
    assert cleaning_right(['hello', 'hell', 'welcofecofefeco']) == ['hell', 'hell', 'wel']
    print("Everything's fine!")


if __name__ == '__main__':
    main()