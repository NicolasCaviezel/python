""" This program has been adapted for use by GVAHIM
       - the main revisions regard pep8 compliance and use of variable names
Copyright 2010 Google Inc.
Licensed under the Apache License, Version 2.0
http://www.apache.org/licenses/LICENSE-2.0

Google's Python Class
http://code.google.com/edu/languages/google-python-class/

Additional basic string exercises  """


# E. verbing
# Given a string, if its length is at least 3,
# add 'ing' to its end.
# Unless it already ends in 'ing', in which case
# add 'ly' instead.
# If the string length is less than 3, leave it unchanged.
# Return the resulting string.
def verbing(some_string):
    if len(some_string) >= 3:
        if some_string[-3:] == "ing":
            return some_string + "ly"
        else:
            return some_string + "ing"
    return some_string

# F. not_bad
# Given a string, find the first appearance of the
# substring 'not' and 'bad'. If the 'bad' follows
# the 'not', replace the whole 'not'...'bad' substring
# with 'good'.
# Return the resulting string.
# So 'This dinner is not that bad!' yields:
# This dinner is good!
def not_bad(some_string):
    # Computation of the 1st index of appearance of 'not' and 'bad'
    not_first_index = len(some_string.split("not")[0]) if len(some_string.split("not")) > 1 else -1
    bad_first_index = len(some_string.split("bad")[0]) if len(some_string.split("bad")) > 1 else -1
    # If there is a not and bad, and not is before than bad make the replacement
    if not_first_index != -1 and bad_first_index != -1 and not_first_index < bad_first_index:
        return some_string[:not_first_index] + "good" + some_string[bad_first_index+3:]
    return some_string


# G. front_back
# Consider dividing a string into two halves.
# If the length is even, the front and back halves are the same length.
# If the length is odd, we'll say that the extra char goes in the front half.
# e.g. 'abcde', the front half is 'abc', the back half 'de'.
# Given 2 strings, input1 and input2, return a string of the form
#  input1-front + input2-front + input1-back + input2-back
def front_back(first_string, second_string):
    # Defining a function to half_split the string according to instructions
    def half_split(string):
        if len(string) % 2 == 0:
            return string[:len(string)//2], string[len(string)//2:]
        return string[:len(string)//2+1], string[len(string)//2+1:]

    first_string_front, first_string_back = half_split(first_string)
    second_string_front, second_string_back = half_split(second_string)

    return first_string_front + second_string_front + first_string_back + second_string_back



def test(got, expected):
    """ simple test() function used in main() to print
        what each function returns vs. what it's supposed to return. """

    if got == expected:
        prefix = " OK "
    else:
        prefix = "  X "
    print("%s got: %s expected: %s" % (prefix, repr(got), repr(expected)))


def main():
    """ main() calls the above functions with interesting inputs,
        using test() to check if each result is correct or not. """

    print("\nverbing")
    test(verbing("hail"), "hailing")
    test(verbing("swiming"), "swimingly")
    test(verbing("do"), "do")

    print("\nnot_bad")
    test(not_bad("This movie is not so bad"), "This movie is good")
    test(not_bad("This dinner is not that bad!"), "This dinner is good!")
    test(not_bad("This tea is not hot"), "This tea is not hot")
    test(not_bad("It's bad yet not"), "It's bad yet not")

    print("\nfront_back")
    test(front_back("abcd", "xy"), "abxcdy")
    test(front_back("abcde", "xyz"), "abcxydez")
    test(front_back("Kitten", "Donut"), "KitDontenut")


if __name__ == "__main__":
    main()
