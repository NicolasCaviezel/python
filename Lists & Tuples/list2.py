""" This program has been adapted for use by GVAHIM
       - the main revisions regard pep8 compliance and use of variable names

Copyright 2010 Google Inc.
Licensed under the Apache License, Version 2.0
http://www.apache.org/licenses/LICENSE-2.0

Google's Python Class
http://code.google.com/edu/languages/google-python-class/

Additional basic list exercises """


# D. Given a list of numbers, return a list where
# all adjacent == elements have been reduced to a single element,
# so [1, 2, 2, 3] returns [1, 2, 3]. You may create a new list or
# modify the passed in list.


def remove_adjacent(nums):
    reduced_nums = []
    previous_num = None
    for num in nums:
        if num != previous_num:
            reduced_nums.append(num)
            previous_num = num
    return reduced_nums


def remove_adjacent_bis(nums:list):
    """

    :param nums: list of possibly redundant numbers that we want to reduce
    :return: a reduced version of number where all elements appears only one time
    """
    # Set object built from lists return a type Set object containing the reduced list
    # Hence casting the resulting set solves the problem
    return list(set(nums))

# E. Given two lists sorted in increasing order, create and return a merged
# list of all the elements in sorted order. You may modify the passed in lists.
# Ideally, the solution should work in "linear" time, making a single
# pass of both lists.
#
# NOTE - DO NOT use return sorted(sorted1 + sorted2) - that's too easy :-)
#


def linear_merge(sorted1:list, sorted2:list):
    """

    :param sorted1: 1st list in increasing order
    :param sorted2: 2nd list in increasing order
    :return: sorted concatenation of sorted1 and sorted2
    """
    idx_sorted1, idx_sorted2 = 0, 0
    merged_list = [None] * (len(sorted1) + len(sorted2))

    # Going through both lists in O(len(sorted1)+len(sorted2)) until we reached the sum of lengths
    while idx_sorted1 + idx_sorted2 != len(merged_list):
        # If we haven't reach the end of sorted1
        # We check wether we need to fill the merged_list with sorted1 s element or sorted2 s element
        if idx_sorted1 < len(sorted1):
            if idx_sorted2 < len(sorted2):
                # sorted1 s current element is less than sorted2 s current element => fill merged_list with sorted1s
                # sorted2 s current element is less than sorted1 s current element => fill merged_list with sorted2s
                if sorted1[idx_sorted1] <= sorted2[idx_sorted2]:
                    merged_list[idx_sorted1 + idx_sorted2] = sorted1[idx_sorted1]
                    idx_sorted1 += 1
                else:
                    merged_list[idx_sorted1 + idx_sorted2] = sorted2[idx_sorted2]
                    idx_sorted2 += 1
            # In the edge case of reaching the end of sorted2 before sorted1 we can fill merged_list with
            # the end of sorted1 and return merged_list
            else:
                merged_list[idx_sorted1 + idx_sorted2:] = sorted1[idx_sorted1:]
                return merged_list
        # Edge case of reaching the end of sorted1 before sorted2 we can fill_merged_list with the end
        # of sorted2 and return merged_list
        elif idx_sorted2 < len(sorted2):
            merged_list[idx_sorted1 + idx_sorted2:] = sorted2[idx_sorted2:]
            return merged_list

def test(got, expected):
    """ simple test() function used in main() to print
        what each function returns vs. what it's supposed to return. """
    if got == expected:
        prefix = " OK "
    else:
        prefix = "  X "
    print("%s got: %s expected: %s" % (prefix, repr(got), repr(expected)))


def main():
    """ main() calls the above functions with interesting inputs,
        using test() to check if each result is correct or not. """

    print("\nremove_adjacent")
    test(remove_adjacent([1, 2, 2, 3]), [1, 2, 3])
    test(remove_adjacent([2, 2, 3, 3, 3]), [2, 3])
    test(remove_adjacent([]), [])

    print("\nlinear_merge")
    test(linear_merge(["aa", "xx", "zz"], ["bb", "cc"]), ["aa", "bb", "cc", "xx", "zz"])
    test(linear_merge(["aa", "xx"], ["bb", "cc", "zz"]), ["aa", "bb", "cc", "xx", "zz"])
    test(linear_merge(["aa", "aa"], ["aa", "bb", "bb"]), ["aa", "aa", "aa", "bb", "bb"])


if __name__ == "__main__":
    main()
