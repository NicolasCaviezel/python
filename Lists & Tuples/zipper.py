def zipper(element1, element2):
    """

    :param element1: tuple or list that we want to zip with element2
    :param element2: tuple or list that we want to zip with element1
    :return: zipped version of element1 element2: that is, a list of tuples containing 1 element from both inputs
    """
    return [(element1[i], element2[i]) for i in range(min(len(element1), len(element2)))]
