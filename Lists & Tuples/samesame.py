def samesame(input_list):
    """

    :param input_list: a list of type which support overloaded + operation (int/char/lists/string)
    :return: the overloaded sum of the lists elements
    """
    # If the list is empty we just return nothing
    if len(input_list) == 0:
        return None
    # Otherwiser to return the proper type we sum from the first element
    # Rk: it works because '+' operator have been overloaded for many objects type
    res = input_list[0]
    for element in input_list[1:]:
        res += element
    return res